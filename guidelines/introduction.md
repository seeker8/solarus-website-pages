# Introduction

## Concept

As you may already know, there is no database in our website, but only text files.

The engine, named [Kokori](https://gitlab.com/solarus-games/kokori), processes these files and convert them to pages. This allows easy contribution and easy versioning. You do not need to understand how Kokori works to contribute to the website.

## Files used

The files used are:
* Markdown files
* JSON files
* Image files (PNG, JPEG, etc.)

We have chosen these file formats because they are easy to read and commonly used.

### Markdown files

Markdown files are simple text files. They are easy to write and read, and are converted automatically to HTML when the engine processes them. If you don't know Markdown, you should [learn the syntax](https://commonmark.org/help/) before editing the files. Don't worry, it's really simple.

We use the Markdown files for the pages content: titles, subtitles, texts, images.

We extended Markdown with **shortcodes**, which are special text, usually written between brackets, to provide more advanced functionnalities like adding a Youtube video to a page, or a particular layout with columns and lines.

### JSON files

JSON files are a bit more complex since they are a structured document. You can see them as a table with 2 columns: keys and values. Values can be nested tables themselves. If you don't know JSON, you should [read about it](https://en.wikipedia.org/wiki/JSON) before editing the files. Don't worry again, it's not complicated.

We use the JSON files for the pages metadata and configuration.

## How it works

TODO Explanation about entities and pages.
