[container]

# {title}

[space]

[row]

[column]

## For players

![Launcher](images/launcher_miniature.png)

[space thickness="20"]

This package contains everything you need to **launch Solarus quests**. If you only want to play games, this is the right package for you.

[align type="center"]
[button-download type="primary" label="Download" url="#" icon="download" icon-category="fa" icon-size="1x" category="player" modal-title="Download" modal-button-label="Download"]
[/align]

### Package content

[box]
[icon-solarus icon="solarus" size="32"] [space orientation="vertical" thickness="5"] **Solarus:** the Solarus core engine.<br/>
[icon-solarus icon="launcher" size="32"] [space orientation="vertical" thickness="5"] **Solarus Launcher:** the application to select and run a game.

[/box]

### How to play

Just [download the quests](/en/games) you want and open them in
Solarus Launcher.

[/column]

[column]

## For game developers

![Quest Editor](images/quest_editor_miniature.png)

[space thickness="20"]

This is the package for people who want to **create Solarus quests**. It expands the player's package with game-making tools.

[align type="center"]
[button-download type="primary" label="Download" url="#" icon="download" icon-category="fa" icon-size="1x" category="quest-maker" modal-title="Download" modal-button-label="Download"]
[/align]

### Package content

[box]
[icon-solarus icon="solarus" size="32"] [space orientation="vertical" thickness="5"] **Solarus:** the Solarus core engine.<br/>
[icon-solarus icon="launcher" size="32"] [space orientation="vertical" thickness="5"] **Solarus Launcher:** the application to select and run a game.<br/>
[icon-solarus icon="editor" size="32"] [space orientation="vertical" thickness="5"] **Solarus Quest Editor:** the game editor.<br/>
[icon-solarus icon="quest" size="32"] [space orientation="vertical" thickness="5"] **Sample Quest:** a very short sample game.

[/box]

### Get started

Tutorials and docs will guide you to make your own Solarus quest.

* [Solarus tutorial](/en/development/tutorials)
* [Solarus documentation](http://www.solarus-games.org/doc/latest)

[/column]

[/row]

## License

[row]
[column width="3"]
[align type="center"]
![GPL v3 logo](images/gpl_v3_logo.png "GPL v3 logo")
[/align]
[/column]
[column]
Solarus is **free software**; you can redistribute it and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
[/column]
[/row]
[space thickness="30"]
[row]
[column width="3"]
[align type="center"]
![CC logo](images/cc_logo.png "CC logo")
[/align]
[/column]
[column]
Solarus also contains **free assets**, licensed under the terms of the [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0). You are free to adapt and modify them as long as you redistribute them under the same license and you credit the author(s).
[/column]
[/row]

[space thickness="50"]

## Other links

If you are looking for something else:

* [Browse Solarus previous or other releases](http://www.solarus-games.org/downloads/solarus/)
* [How to compile Solarus by yourself.](https://gitlab.com/solarus-games/solarus/blob/dev/compilation.md)
* [Source code of all Solarus Team projects](https://gitlab.com/solarus-games)

[/container]
