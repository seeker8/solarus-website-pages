[container layout="small"]
[align type="center"]

# {title}

[hr width="10"]

## A 2D game engine for Action-RPGs

[hr width="40"]

[space thickness="20"]

[youtube id="vt07FwzLo9A"]

[space thickness="30"]

Solarus was specifically designed with cult 2D action-RPG classics in mind, such as **The Legend of Zelda: A Link to the Past** and **Secret of Mana** on the Super Nintendo, or **Soleil** on the Sega Megadrive/Genesis.

The engine is programmed in **C++**, with the **SDL** library and an **OpenGL** backend. Games made with Solarus are called **quests**, and are programmed in **Lua**.

The engine does all the **heavy computations** (for example, collision checks) and the **low-level operations** like drawing the screen, animating sprites and playing sounds.

As a quest maker, you are not much interested in implementing these algorithms. On the contrary, you want to define the **game logic**. Your Lua scripts describe the behavior of enemies, what happens when pressing a switch on a specific map. They will also implement such things as the title screen and the head-up display.

Both parts (the C++ engine and the Lua scripts of your quest) communicate through the **Solarus Lua API**. The communication works in both ways: you can call functions of the engine (example: you want to move a non-playing character) and the engine calls your own functions (example: be informed that an enemy was just killed). But before using this Solarus API, you have to learn the basics of Lua (easy and minimal, yet powerful language).

[space]

[row]
[column]

#### C++ core

![C++ logo](images/cpp_logo.png "C++ logo")

[/column]
[column]

#### Lua API

![Lua logo](images/lua_logo.png "Lua logo")

[/column]
[column]

#### Shaders

[space thickness="15"]
![OpenGL logo](images/opengl_logo.png "OpenGL logo")

[/column]
[/row]

[space thicknesss="30"]

[/align]
[/container]

[highlight]
[container]
[align type="center"]
[space thickness="10"]

## Tools for players and developers

[hr width="20"]
[row]
[column]

### For players

[/column]
[column]

### For quest makers

[/column]
[/row]

[row]
[column]
![Launcher miniature](images/launcher_miniature.png "Launcher miniature")
[space thickness="20"]
![Launcher logo](images/launcher_logo.png "Launcher logo")
[/column]
[column]
![Quest editor miniature](images/quest_editor_miniature.png "Quest editor miniature")
[space thickness="20"]
![Quest editor logo](images/quest_editor_logo.png "Quest editor logo")
[/column]
[/row]

[row]
[column]
[space thickness="20"]
A library manager and launcher tailored for Solarus-made games.
[/column]
[column]
[space thickness="20"]
A graphical editor for maps, sprites, dialogs, and Lua and GLSL scripts, all in one.
[/column]
[/row]
[space]
[/align]
[/container]

[space]

[container]
[row]
[column]
[image url="images/screenshot-quest-editor-01.png" alt="Screenshot 1"]
[/column]
[column]
[image url="images/screenshot-quest-editor-02.png" alt="Screenshot 2"]
[/column]
[column]
[image url="images/screenshot-quest-editor-03.png" alt="Screenshot 3"]
[/column]
[column]
[image url="images/screenshot-quest-editor-04.png" alt="Screenshot 4"]
[/column]
[/row]
[/container]

[/highlight]

[container]
[align type="center]
[space thickness="10"]

## Multi-platform

[hr width="10"]

Solarus is available on a great number of platforms, including Windows, macOS, Linux, BSD and even Android (coming soon).

[space thickness="10"]

[row]
[column]
[icon icon="windows" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="apple" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="android" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="ubuntu" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="freebsd" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="suse" category="fab" type="default" size="4x"]
[/column]
[column]
[icon icon="raspberry-pi" category="fab" type="default" size="4x"]
[/column]
[/row]
[space]
[/align]
[/container]

[container layout="small"]
[align type="center]
[space thickness="10"]

## Free and open-source

[hr width="20"]

[row]
[column]
![GPL v3 logo](images/gpl_v3_logo.png "GPL v3 logo")
[/column]
[column]
![CC logo](images/cc_logo.png "CC logo")
[/column]
[/row]

[space]

Solarus is free software; you can redistribute it and/or modify it under the terms of the **GNU General Public License** as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Included assets are also free; they are licensed under a **Creative Commons Attribution-ShareAlike 4.0 international license** (CC-BY-SA 4.0).

[space]

[/align]
[/container]

[highlight]
[container layout="small"]
[align type="center]

[space thickness="10"]

## Get started

[hr width="10"]

Ready to create your first Solarus quest?

[button type="outline-primary" label="View code" url="https://gitlab.com/solarus-games" icon="gitlab" icon-category="fab" icon-size="1x"]
[space orientation="vertical" thickness="40"]
[button type="primary" label="Download" url="/en/solarus/download" icon="download" icon-category="fa" icon-size="1x"]

[space thickness="70"]

[/align]
[/container]
[/highlight]
