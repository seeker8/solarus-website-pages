# {title}

[youtube id="WpSp7_F0SIM"]

## Summary

- Overview of Lua scripts
- Using the Lua API documentation
- The `gamemanager.lua` file
- The `main.lua` file
- Events
- Playing audio

## Resources

- Video made with Solarus 1.5.
- [Download Solarus](/solarus/download)
- [Solarus documentation](https://www.solarus-games.org/doc/latest/)
- [How to program in Lua](http://www.lua.org/pil/contents.html)
- [Legend of Zelda: A Link to the Past resource pack](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
