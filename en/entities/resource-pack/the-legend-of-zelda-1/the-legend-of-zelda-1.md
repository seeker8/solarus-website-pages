### Presentation

*The Legend of Zelda*, released in 1986 on Nintendo Entertainment System, is the first episode of the massively influential saga that will ultimately led to the creation of Solarus.

This resource pack is being gathered progressively [on the forum](http://forum.solarus-games.org/index.php/topic,1365.0.html), and let's hope someday it'll be possible to rebuild this seminal video game with Solarus.

![The Legend of Zelda NES box](images/box_art.png)
