That was a promise!

It's been a long time that we were talking about them and they were much requested: [tutorials to learn how to use Solarus](/en/development/tutorials) are now live on the website!

Tutorials will present like this:

- It is possible to create virtual books, featuring a set of chapters and pages within.
- Currently, there are two books: Christopho's [video tutorial](/en/development/tutorials/solarus-video-tutorial), and the new [text tutorial](/en/development/tutorials/solarus-official-guide), not as furnished as the video one.
- Everything is based on markdown files, indexed in the [website pages' Gitlab repository](https://gitlab.com/solarus-games/solarus-website-pages).
- Everyone can fork, edit and/or add pages, then create a pull request to add modifications to the website.

Thanks to Binbin to get all this system working. We hope that contributions will be numerous in order to develop the official guide, with chapters about basics or more advanced features brought by Solarus 1.6.
