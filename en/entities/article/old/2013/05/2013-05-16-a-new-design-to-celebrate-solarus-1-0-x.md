As you may have seen, Solarus 1.0.1 has just been released. The project is becoming quite big, as it incorporates an engine, 2 games (3 games soon) and a quest editor. With the aim of increasing the project's visibility, an official logo has been made. We hope it will remind you vintage 90's video games companies logos. You can freely use it (both png and pixel-art) for your project as it is under <a title="Creative Commons License 3.0" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons license</a>, and download the <a title="full pack" href="http://www.solarus-games.org/wp-content/uploads/2013/05/Logo-Solarus-Engine.7z">full pack</a> of logos in EPS and PNG formats on the <a title="Logo" href="http://www.solarus-games.org/solarus/logo/">Solarus logo page</a>.

[caption id="attachment_496" align="aligncenter" width="300" caption="Solarus Engine logo to use on light backgrounds (click for full size)"]<a href="http://www.solarus-games.org/wp-content/uploads/2013/05/solarus-logo-black-on-transparent1.png"><img class="size-medium wp-image-496   " title="solarus-logo-black-on-transparent" src="/images/solarus-logo-black-on-transparent1-300x90.png" alt="" width="300" height="90" /></a>[/caption]

[caption id="attachment_497" align="aligncenter" width="300" caption="Solarus Engine logo with black background (click for full size)"]<a href="http://www.solarus-games.org/wp-content/uploads/2013/05/solarus-logo-white-on-black.png"><img class="size-medium wp-image-497" title="solarus-logo-white-on-black" src="/images/solarus-logo-white-on-black-300x90.png" alt="" width="300" height="90" /></a>[/caption]

[caption id="attachment_498" align="aligncenter" width="231" caption="Solarus Engine logo in pixel-art, for title screensSolarus Engine logo for title screens"]<a href="/images/title_screen_solarus_engine.png"><img class="size-full wp-image-498 " title="title_screen_solarus_engine" src="/images/title_screen_solarus_engine.png" alt="Solarus Engine logo in pixel-art, for title screens" width="231" height="78" /></a>[/caption]

Consequently, the design for this blog has also been remade to match the logo's style. It uses the Presswork Wordpress theme which is a great theme if you want to build up quite fast and easily a blog. The pages have also been remade with more illustrations to improve navigation and readability.
<h2>Changes</h2>
<ul>
	<li>New Wordpress theme : Presswork</li>
	<li>Responsive design</li>
	<li>Colors and design match the new logo style</li>
	<li>Illustrations, logos and artworks</li>
	<li>Christopho's tweets (at the bottom of the page)</li>
	<li>Page to download Solarus logo</li>
</ul>

We hope you'll like it. And now, here is a small bonus :

[caption id="attachment_506" align="aligncenter" width="320" caption="Animated title screen for Solarus logo"]<a href="/images/titre-solarus-anim2.gif"><img class="size-full wp-image-506" title="titre-solarus-anim2" src="/images/titre-solarus-anim2.gif" alt="" width="320" height="240" /></a>[/caption]