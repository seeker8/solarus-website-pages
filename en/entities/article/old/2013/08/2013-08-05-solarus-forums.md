Since the Solarus 1.0 release, several people suggested that we create a forum.

Here it is! <a href="http://forum.solarus-games.org/index.php">http://forum.solarus-games.org/</a>

We hope that the forum will be very useful for people who use the Solarus engine. You will be able to talk about developing your own games, get help, share knowledge, scripts and game resources. It will also be a place to present your projects to the community. We are open to any suggestion!