Christopho began to make video tutorials about how to make a game with Solarus. However, these tutorials are, for the moment, only in French. To help the community to get the basis of Solarus, a <a href="http://wiki.solarus-games.org/">Wiki </a>where everyone can contribute has been set up. We use <a href="https://www.dokuwiki.org/">Dokuwiki</a>, a free and open-source file-based wiki engine.
<ul>
	<li><a href="http://wiki.solarus-games.org/">Solarus wiki</a></li>
</ul>
Currently there is almost nothing on the Wiki, but we are working on it, and we hope you too !