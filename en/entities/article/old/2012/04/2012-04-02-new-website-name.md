Hi,

The English translation of <a title="Zelda Mystery of Solarus DX" href="http://www.solarus-games.org/games/zelda-mystery-of-solarus-dx/">Zelda Mystery of Solarus DX</a> is almost done! I am currently testing the game in English to make a last check.

The game will probably be released in English in a few days. Some changes will appear on this website. First of all, the url has changed: it is now www.solarus-games.org. I did not like the old one :) And we talk about our games, not only about the engine. After the release, I will add a download page and the navigation menus will also be improved.

Stay tuned!