## A brand new website, with a custom engine

The previous website, made with Wordpress, began to show its age. Wordpress was a bit unwieldy, and we wanted an way to involve people into the project.

It has been a long time since the project had been planned, but we hadn't had time and skills to do it. Mockups began early in 2013. Here comes Binbin, a longtime friend of Christopho, who previously helped in game development. He wrote a website engine, with Koseven framework, that used Markdown files as basis. We wanted to create our own engine to perfectly fit to our needs and have total freedom.

![Kokori logo](2019-04-01-new-website-release/kokori_logo.png)

The engine is named **Kokori**, which is a wordplay between *Kokiri* (from the *Legend of Zelda* series - you should begin to know that we like it, right?),  the framework *Koseven* which it is based on, and *coq-au-riz* (cock with rice), a French dish Binbin really likes. It is free and open-source, and the website's data is separated from the engine. The data is also free and open-source.

* [Website engine source code](https://gitlab.com/solarus-games/kokori)
* [Website data source code](https://gitlab.com/solarus-games/solarus-website-pages)

The website features several improvements :

* **A quest library:** to list all the (completed) games made with Solarus, with searching and filtering.
* **Quest pages:** to show off the games in a clean and pro way, and allow to download the quest data.
* **A resource pack library:** packs of tilesets, sprites, musics, sounds and scripts, very useful for quest makers.
* **Tutorials:** Tutorials to learn how to create a quest with Solarus!
* A lot of stuff has been improved, to make the website easier to update, so that we can spend less time on maintaining the website, and more time on developing Solarus and our games.

In the future, we would like the Quest Library to be available with an API, to allow Android and desktop Quest Launchers to fetch this library, and make downloading quests easier for the player.

The engine and website are both really young, so pleast let us know if you find any bug, on the respective Gitlab projects.

## We need your help

Now, modifications will be made with the classic Git way: branches and pull requests. Anyone can contribute to improve the website, though Binbin will have the final word on whether to merge or not. We need contributors for the following parts:

* **Kokori:** if you feel you can improve the engine, Binbin will gladly welcome you.
* **Translations:** anyone can translate the website in other languages.
* **Tutorials:** we need help to write Solarus tutorials for quest makers.

How to contribute? You will find the info needed on the [contribution page](/en/development/how-to-contribute). Don't hesitate to come over on [Discord](https://discord.gg/yYHjJHt) to chat with us and offer your help.

Guidelines are in the repository of the website data. Helping is pretty simple because it is only Markdown. The syntax is easy to apprehend, and markdown files are really easy to read even without formatting.

![Tutorial icon](2019-04-01-new-website-release/tutorial_icon.png)

## The old French website

French-speakers and longtime followers, please know that [Zelda-Solarus](http://www.zelda-solarus.com/zs/), the original French website where all started, is now merged with Solarus-Games. For those who do not know, it was a website about both our amateur *Zelda* fan-games and official ones, featuring lots of fan-content, walkthroughs, and an incredibly active forum in its golden days.

![Zelda-Solarus website](2019-04-01-new-website-release/zelda_solarus_website.png)

We decided to stop focusing on official *Zelda* games, and focus only on engine and game development. All the official *Zelda* content will not be lost, because [Zeldaforce](https://zeldaforce.net/), Binbin's website about *Zelda*, still exists, and picks up the essential Zelda-related content that was on Zelda-Solarus.

In short, Zelda-Solarus is becoming the French version of Solarus-Games. And keep in mind that the old French forums are not moving!

## Conclusion

Now, Solarus has a clean and nice looking website, and is ready to grow its community! This is the end of an era and the beginning of a new one, my friends!
