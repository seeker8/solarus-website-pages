Two years ago, in 2017, ZeldoRetro made his first game with Solarus: the first chapter of his series *Le Défi de Zeldo* (Zeldo's Challenge), named *La Revanche du Bingo* (Bingo's Revenge). But at the time, it was only a little challenge for his friend Adenothe, not aimed to be publicly released. However, the release of the second chapter made him consider releasing the first one, because everyone was asking "Wait? If this is chapter 2, where is chapter one?". Here it is!

![Logo](2019-09-24-zeldo-ch-1-game-release/game-logo.png)

The game was much less ambitious than its sequel. It contains only one dungeon, and no overworld to explore, which makes the game rather short. You may beat it under an hour.

![Screenshot 1](2019-09-24-zeldo-ch-1-game-release/screen_1.png)

![Screenshot 2](2019-09-24-zeldo-ch-1-game-release/screen_2.png) 

You can download the game on [its page in the Solarus Quest Library](/en/games/defi-de-zeldo-ch-1).

[![Download](2019-09-24-zeldo-ch-1-game-release/game-download.png)](/en/games/defi-de-zeldo-ch-1)
