[container]

# {title}

## Histoire

Tout a commencé il y a bien longtemps, en 2001 pour être exact, quand Christopho, le créateur et principal contributeur de Solarus, a lancé un site Internet pour son premier projet de jeu amateur : **Zelda-Solarus**. C'était un moyen de promouvoir la toute première version de **Mystery of Solarus**, un jeu réalisé avec RPG Maker et imaginé comme étant une suite à *The Legend of Zelda: A Link to the Past*.

Au fur et à mesure, le site et son forum ont grossi, et sont devenus un lieu attirant non seulement les développeurs de jeux à la Zelda, mais aussi les fans de Zelda, puisque le site était également pourvu de mangas, soluces, tests, ressources et actualités à propos de cette série de jeux vidéo.

Christopho décida en 2006 de refaire en C++ son premier jeu, qui devint alors [Mystery of Solarys DX](/fr/games/the-legend-of-zelda-mystery-of-solarus-dx), et **Solarus** était né.

Il fut par la suite décidé que le moteur méritait d'avoir son propre site Internet. Les deux sites ont longtemps coexisté, avant de fusionner en 2019 pour donner le **site Solarus-Games** que vous connaissez aujourd'hui.

L'équipe originelle, aidée de nouveaux membres, a continué de faire des jeux avec le moteur, sous la bannière **Solarus Team**, indépendamment du travail sur le moteur.

Le projet prend aujourd'hui de l'ampleur et devient un moteur de jeu Action-RPG généraliste. De plus en plus de personnes ont rejoint la communauté, et nous avons la chance de recevoir de nombreuses contributions. Au point qu'une association nommée [Solarus Labs](/fr/about/nonprofit-organization) a été créée afin de promouvoir le projet et recevoir plus facilement des dons.

## Contributeurs

### Principaux contributeurs actifs

Solarus est développé par une communauté de passionnés bénévoles qui contribuent au code, aux rapports de bugs, à la documentation, aux illustrations, au support, etc. sur leur temps libre, comme hobby. Vous pouvez utiliser cette liste pour savoir qui contacter à propos d'une partie spécifique du projet.

* [Christopho](https://twitter.com/ChristophoZS) (Founder, Lead Developer)
* [Std::Gregwar](https://gitlab.com/stdgregwar) (Co-lead Developer)
* [Hugo Hromic](https://gitlab.com/hhromic) (DevOps)
* [Olivier Cléro](https://twitter.com/olivclr) (Artwork, Communication)
* [Binbin](https://twitter.com/zelda_force) (Web Developer)

### Tous les contributeurs

Il est impossible de lister tous les contributeurs. Néanmoins, cette liste donne, dans l'ordre alphabétique, les gens qui ont contribué de façon significative au projet (apport supérieur à 10 commits). Merci à chacun d'entre eux, ils ont participé à faire de Solarus ce qu'il est aujourd'hui !

* 19oj19
* Alex Gleason
* BenObiWan
* Binbin
* Clem04
* Cluedrew Kenfar Ink
* Christopho
* Diarandor
* Hugo Hromic
* Max Mraz
* Maxs
* Metallizer (Co-founder)
* Michel Hermier
* Morwenn
* Mymy
* Nathan Moore
* Newlink
* Olivier Cléro
* Perry Thompson
* Peter De Wachter
* PhoenixII54
* Renkineko
* Samuel Lepetit
* Sergio C
* Shin-NiL
* Std::gregwar
* Vlag
* Xethm55
* Zane Kukta
* 高彬彬

### Donateurs

* Alex Gleason (100€)
* Adrian Moga (50€)
* 6S-Studio (25€)
* David Wisser (20€)
* EthanGold60 (5€)

[/container]
