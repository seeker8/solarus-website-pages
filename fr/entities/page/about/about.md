[container]

# {title}

[row]
[column]

[button-highlight type="primary" icon="question" url="about/faq" label="Foire Aux Questions"]

[/column]
[column]

[button-highlight type="primary" icon="team" url="about/team" label="Équipe"]

[/column]
[column]

[button-highlight type="primary" icon="foundation" url="about/nonprofit-organization" label="Association"]

[/column]
[/row]

[row]
[column]

[button-highlight type="primary" icon="info" url="about/legal" label="Legal Information"]

[/column]
[column]

[button-highlight type="primary" icon="mail" url="about/contact" label="Contact"]

[/column]
[column]
[/column]
[/row]

[/container]