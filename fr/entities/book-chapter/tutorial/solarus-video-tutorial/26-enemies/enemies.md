# {title}

[youtube id="h3g2PAyUl6Q"]

## Sommaire

- Sprite d'ennemi tué
- Placer un ennemi sur une map
  - Skeletor
- Faire en sorte qu'un ennemi lâche un trésor lorsqu'il est tué
- Créer un script d'ennemi
  - Mettre à jour la détection du sprite de l'ennemi pendant qu'il se déplace

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
- [Ressources utiles pour reproduire ce chapitre](https://www.solarus-games.org/tuto/fr/basics/ep26_ressources.zip)
