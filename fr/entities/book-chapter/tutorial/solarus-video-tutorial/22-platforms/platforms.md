# {title}

[youtube id="vvQEM0NfZCs"]

## Sommaire

- Utiliser les escaliers de plateforme dans un donjon à plusieurs niveaux
- Comment éviter les problèmes avec les jumpers dans le coin intérieur d'une plateforme
- Placer un mur derrière les escaliers de plateform pour bloquer le héros

## Resources

- Video réalisée avec Solarus 1.5.
- [Télécharger Solarus](/solarus/download)
- [Documentation de Solarus](https://www.solarus-games.org/doc/latest/)
- [Apprendre à programmer en Lua](http://www.lua.org/pil/contents.html)
- [Pack de resources pour Legend of Zelda: A Link to the Past](/development/resource-packs/the-legend-of-zelda-a-link-to-the-past)
