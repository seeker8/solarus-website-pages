<p>Salut à tous !</p>

<p>Ce week-end a été marqué par la reprise concrète de la programmation du jeu. En effet il faut bien avouer que je n'ai pas trouvé le temps de faire grand chose pendant ces courtes vacances. Mais avant de vous parler du jeu, je voulais vous rassurer au sujet de la news de Netgamer du 6 janvier ("Risque important"). Son problème de forfait est résolu et le site n'est plus menacé.</p>

<p>Pour revenir au jeu, l'information du jour est que je viens de terminer le niveau 7 ! Il est donc prêt à être testé (notamment par Netgamer, qui ne manquera pas de vous donner ses impressions).</p>

<p>Quant à la date de sortie finale, elle est toujours fixée au mois de mars / avril. Tout dépend du temps que nous mettrons pour finir le jeu, et surtout pour le tester.</p>