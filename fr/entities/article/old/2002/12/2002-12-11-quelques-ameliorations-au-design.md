<p>Comme vous pouvez le voir, l'interface du site a été quelque peu modifiée. En effet le nouveau design rendait la navigation plus compliquée à cause des menus en moins. Nous avons donc réorganisé cela différemment.</p>

<p>D'abord, le menu des jeux a été remis car la barre de navigation avec les icônes des jeux ne suffisait pas. Le menu "A l'affiche" a été conservé mais uniquement sur la page d'accueil. En effet il sert à proposer des liens rapides vers certaines rubriques du site, c'est donc surtout sur la page d'accueil qu'il est utile.</p>

<p>Nous avons ainsi gagné de la place à gauche, ce qui nous a permis de remettre un menu "le site". Par ailleurs, le menu du haut a été totalement modifié pour proposer désormais simplement des liens vers l'accueil, Zelda Solarus, Advanced Project, et les forums.</p>

<p>Le look des mises à jour de la page d'accueil a lui aussi été refait. Et enfin, sur chaque page des jeux, les rubriques sont désormais présentées dans un tableau sur la droite au lieu d'être en haut de la page comme avant.</p>

<p>On espère que ces améliorations vous plairont !</p>