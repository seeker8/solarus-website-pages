Cette annonce est le démenti de l'annonce précédente. Ne tenez pas compte de la précédente news intitulée &quot;Poisson d'avril&quot;, car nous étions encore le 1er avril lorsqu'elle a été publiée ;)

[b]Ce qui est vrai et ce qui est faux :[/b]

En fait, le seul truc qui était faux, c'était bien sûr Christofaux, qui est le vrai (il n'y en a qu'un).

Finalement, presque tout était vrai. [b]Il y a bien un remake de Zelda Solarus[/b] en cours de création ! Les captures d'écran sont absolument authentiques, c'est d'ailleurs pour ça qu'elles paraissent basiques : c'est parce que très peu de maps sont déjà faites, et pour arriver à ce résultat, il a fallu développer l'éditeur de maps, qui peut être considéré comme un projet à lui tout seul. L'écran des sauvegardes est volontairement repris de Mercuris' Chest, de même que l'écran de jeu (coeurs, rubis...) qui n'est d'ailleurs pas encore fini.
Les 400 heures de travail et les 10000 lignes de code, c'est vrai aussi. Ca représente 5 à 10 heures de travail par semaine, depuis plus d'un an. Quand on dit qu'un projet de jeu amateur demande énormément de temps, c'est pas des blagues :lol:

Merci à Metallizer qui a eu l'idée de cette fausse annonce postée peu avant minuit :mrgreen:

[b]Un retour aux sources[/b]

Une page est en train de se tourner dans l'histoire du site Zelda Solarus. A partir de demain, le site sera consacré en priorité aux nouvelles de la création du projet. Le site continuera à parler des Zelda, mais dans une moindre mesure, d'autant que l'actualité est assez faible en ce moment. Comme à ses débuts, je ferai des mises à jour fréquentes pour expliquer l'avancement du projet au fur et à mesure de son développement. Les premiers changements seront visibles dès demain ^_^