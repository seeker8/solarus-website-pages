Vous les attendiez avec impatience : deux nouveaux épisodes de la soluce vidéo de [url=http://www.zelda-solarus.com/jeu-zsdx]Zelda Mystery of Solarus DX[/url] viennent d'être publiés.

[list]
[li] [url=http://www.youtube.com/watch?v=x84d8Lh24Mg]Épisode 9 : du niveau 4 au niveau 5[/url][/li]
[li] [url=http://www.youtube.com/watch?v=oeNE68E4xmk]Épisode 10 : niveau 5 - Ancien Château[/url][/li]
[/list]

Nous en sommes à peu près à la moitié du jeu, à la fin du cinquième donjon. Merci à Elenya, qui est en train de traduire le jeu en allemand et qui m'a accompagné aux commentaires pour ces deux épisodes ! :)