En ce nouveau mois de septembre et en cette période troublée par la rentrée des classes, il est temps pour nous de vous donner des <strong>nouvelles du projet A Link to the Dream</strong>.
Pour ceux qui ne le savent pas encore, il s'agit d'un remake de Link's Awakening mais utilisant les graphismes de A Link to the Past. Nous utilisons, pour le mettre en place, le moteur <strong>Solarus 1.6</strong> dont la release devrait sortir théoriquement courant octobre 2018.
Cette nouvelle version du moteur permet vraiment de faire des choses fantastiques, avec notamment le support des shaders OpenGL. Par exemple, nous avons mis en place dernièrement de superbes transitions pour passer d'une carte à une autre. N'hésitez pas à cliquer sur l'image ci-dessous pour voir l'aperçu !
<div class="clearfix"><a href="../data/fr/entities/article/old/2018/09/images/transition.gif"><img class="alignnone size-medium wp-image-38349" src="/images/transition-300x215.gif" alt="" width="300" height="215" /></a></div>
<div></div>
<br/><br/>
Sinon au niveau des nouveautés dans le projet, voici une liste de ce qui a été fait dernièrement :

- Mise en place des sols qui s'effondrent.
- Développement et mise en place de différents ennemis
- Refonte de la seconde mélodie de l'ocarina : le Mambo de Manbo
- Donjon 1 : ce dernier est désormais terminé avec le développement du boss.
- Retouches et finalisation des différents dialogues du jeu entre le début du jeu et le donjon 1.
- Retouches graphiques diverses
- Corrections de différents bugs (le jeu a à présent beaucoup gagné en stabilité)
- Des surprises non dévoilées à l'heure actuelle !

Sachez que nous prenons un malin plaisir à faire un remake de qualité et le moindre détail compte. Pour nous, il est très important de faire découvrir ce chef d'oeuvre pour ceux qui ne le connaissent pas mais aussi de faire plaisir au joueur qui connait le jeu par coeur. J'espère que ce remake sera à la hauteur de vos attentes.

Pour vous faire patienter, nous avons le plaisir de vous montrer aussi plusieurs aperçu du donjon 4 du jeu : l'abîme du poisson. N'hésitez pas à nous faire part de vos remarques et à bientôt dans une prochaine news !
<a href="../data/fr/entities/article/old/2018/09/images/1.png"><img class="alignnone size-medium wp-image-38345" src="/images/1-300x240.png" alt="" width="300" height="240" /></a>

<a href="../data/fr/entities/article/old/2018/09/images/3.png"><img class="alignnone size-medium wp-image-38347" src="/images/3-300x240.png" alt="" width="300" height="240" /></a>

<a href="../data/fr/entities/article/old/2018/09/images/4.png"><img class="alignnone size-medium wp-image-38348" src="/images/4-300x240.png" alt="" width="300" height="240" /></a>

<a href="../data/fr/entities/article/old/2018/09/images/2.png"><img class="alignnone size-medium wp-image-38346" src="/images/2-300x240.png" alt="" width="300" height="240" /></a>
