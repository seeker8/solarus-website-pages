Bonjour à toutes et à tous !

Depuis le temps que vous le réclamiez, et bien ça y est, l'heure est enfin venue de vous dévoiler l'existence des tutoriels écrits pour créer vos propres jeux avec le moteur Solarus !

Le contenu des tutoriels écrits est sensiblement le même que celui des tutoriels vidéos, la différence notable étant que les maps créées n'étant pas contraintes par le temps, elles sont un peu plus esthétiques. De plus, des exercices vous attendent à la fin des tutoriels clés pour vous donner une direction vers laquelle avancer. Ces exercices ne sont pas obligatoires, j'espère juste qu'ils vous permettront de mieux appréhender le moteur et son éditeur de map que de simplement suivre des indications sans réfléchir. Bien évidemment, des corrections sont/seront disponibles pour chaque exercice.

Actuellement, les tutoriels sont écrits uniquement en français, mais si des personnes veulent se saisir de l'occasion pour les traduire qu'ils n'hésitent pas, c'est aussi pour ça que c'est la gestion par wiki qui a été choisie ! N'oubliez pas que vous pouvez aussi visiter le forum du moteur ( http://forum.solarus-games.org ), en anglais lui, pour toute demande d'aide ou si vous voulez juste vous la péter avec vos scripts qui déchirent ;)

<a href="http://wiki.solarus-games.org/doku.php?id=fr:tutorial:create_your_2d_game_with_solarus">Les tutoriels écrits</a>.
Pour rappel, <a href="http://www.youtube.com/playlist?list=PLzJ4jb-Y0ufySXw9_E-hJzmzSh-PYCyG2">les tutoriels vidéos</a>.