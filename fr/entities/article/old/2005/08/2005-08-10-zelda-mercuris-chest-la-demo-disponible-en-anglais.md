Je suis heureux de vous annoncer que comme promis, la d&eacute;mo est maintenant disponible en version anglaise ! La traduction a &eacute;t&eacute; faite par Geomaster. Tout le jeu est donc traduit en Anglais ainsi que le mode d'emploi.

[center]
[img]http://www.zelda-solarus.com/images/zf/notice/en/01.png[/img]
[/center]

[list]
[li][url=jeux.php?jeu=zmc&amp;zone=english]Zelda : Mercuris' Chest d&eacute;mo version anglaise[/url][/li]
[li][url=jeux.php?jeu=zmc&amp;zone=notice&amp;langue=en]Mode d'emploi en Anglais[/url][/li]
[/list]

Nous esp&eacute;rons que cette traduction en Anglais permettra &agrave; Zelda Solarus de se faire mieux conna&icirc;tre dans le monde :)