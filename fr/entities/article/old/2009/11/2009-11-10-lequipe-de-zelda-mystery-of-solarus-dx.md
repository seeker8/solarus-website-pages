A 38 jours de la sortie de la démo de [url=http://www.zelda-solarus.com/jeu-zsdx]Zelda Mystery of Solarus DX[/url], je vous propose aujourd'hui d'en savoir plus sur l'équipe, c'est-à-dire tous ceux qui participent ou ont participé au projet jusqu'ici, et le rôle de chacun.
La page &quot;équipe&quot; vient en effet d'être mise à jour, elle est plus complète et surtout plus détaillée qu'auparavant.
Vous y verrez que si environ 3 membres travaillent de manière quotidienne sur le projet, le total des contributions monte à 27 personnes en comptant les tests, les traductions, les portages et bien sûr ceux qui ont donné un coup de main dans le passé.

[list]
[li][url=http://www.zelda-solarus.com/jeu-zsdx-equipe]L'équipe de Zelda Solarus DX[/url][/li]
[/list]

Merci et bravo à toute l'équipe pour votre aide :)