Pour prolonger l'anniversaire des 7 ans de la sortie du premier [url=http://www.zelda-solarus.com/jeu-zs]Mystery of Solarus[/url], je vous ai réservé une petite surprise : rien de moins une vidéo de [url=http://www.zelda-solarus.com/jeu-zsdx]Mystery of Solarus DX[/url] ! Branchez vos hauts-parleurs ! :D

[center][url=http://www.zelda-solarus.com/zsdx/videos/demo.html][img]http://www.zelda-solarus.com/images/zsdx/village_boy.png[/img][/url][/center]

Cette vidéo vous donne un aperçu de quelques passages du début du jeu. Vous y verrez quelques personnages, le système de combats en action, quelques désagréments dont vous pourriez être victime, mais aussi la nouvelle version de certaines maisons... Je ne vous en dis pas plus, vous verrez bien :P

[list]
[li][url=http://www.zelda-solarus.com/zsdx/videos/demo.html]Voir la vidéo sur Zelda Solarus[/url][/li]
[li][url=http://www.youtube.com/watch?v=r3e7t0QvImY]Voir la vidéo sur Youtube[/url][/li]
[/list]

Bref, j'espère que cela vous permettra de patienter avant la sortie de la démo jouable, qui ira jusqu'à la fin du premier donjon :).