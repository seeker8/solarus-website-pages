<p>Bienvenue sur Zelda Solarus Nouvelle formule !</p>
<p>Beaucoup plus beau qu'avant et beaucoup plus rapide ! Ce nouveau site offrira un grand nombre de services qui seront en ligne d'ici une semaine !</p>
<p>Toutes les options du site sont disponibles et fonctionnent correctement ! Mais si vous trouvez un bug, contactez nous vite !</p>
<p>Vous pouvez toujours laisser des messages sur le <a href="http://www.zelda-solarus.com/forum.php3">forum</a> concernant le jeu ou le site !</p>
<p>Je crois que tout est clairement dit ! Je vous souhaite une agréable visite sur Zelda Solarus !</p>