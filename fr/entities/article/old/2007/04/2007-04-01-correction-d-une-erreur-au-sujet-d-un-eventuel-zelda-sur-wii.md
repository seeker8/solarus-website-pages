Après lecture de certains posts sur le topic concernant la nouvelle d'un éventuel nouveau Zelda en préparation sur Wii, il m'est apparu effectivement quelques incohérences. J'ai donc décidé de contacter mon contact à l'étranger qui m'a fournit de nouvelles informations afin d'avoir des exclusivités pour Zelda-Solarus.

Et effectivement, mon contact a mal interprété ses sources puisqu'il s'agirait en fait de Phantom Hourglass sur DS qui serait concerné par une aventure en mode online.
Voici ce qu'il a pu en tirer:

[center][url=http://www.zelda-solarus.com/images/actu/ph_multi.jpg][img]http://www.zelda-solarus.com/images/actu/ph_multi.jpg[/img][/url][/center]

N'oubliez pas les autres captures de Phantom Hourglass déjà présentes sur le site. 

PS: Je m'excuse de l'erreur de ma précédente nouvelle.

