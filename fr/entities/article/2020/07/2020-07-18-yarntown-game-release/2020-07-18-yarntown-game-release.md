Max Mraz, qui travaille également sur son projet plus ambitieux [Ocean's Heart](/fr/games/oceans-heart), vient tout juste de publier un jeu relativement court nommé **Yarntown**. C'est un hommage au déjà légendaire Bloodborne, originellement sorti sur PS4 en 2015. Ne vous fiez pas aux graphismes mignons comme tout, la difficulté du jeu est brutale et sans pitié, tout comme son inspiration !

- Télécharger le jeu sur [sa page dans la Bibliothèque de quêtes Solarus](/fr/games/yarntown).
- Télécharger le jeu sur [itch.io](https://maxatrillionator.itch.io/yarntown)

![Screenshot 1](images/screen_1.png)

![Screenshot 2](images/screen_2.png)
