Un nouveau jeu fait avec Solarus est disponible depuis le 10/05/2019. Il s'agit du chapitre 2 du *Défi de Zeldo*, intitulé *La Tour des Souvenirs*, et développé par une seule personne : ZeldoRetro !

![Logo](2019-05-19-zeldo-game-release/game-logo.png)

Il s'agit d'un jeu au ton souvent humoristique, comprenant de nombreuses références et personnages issues d'univers différents. La durée de vie est assez courte pour celles et ceux qui ne s'attarderont que sur la quête principale, mais l'intérêt se situe aussi dans les nombreuses quêtes annexes proposées qui donneront du fil à retordre aux plus complétionnistes.

![Screenshot 1](2019-05-19-zeldo-game-release/screen-1.png)

Le jeu fait suite au premier chapitre, que ZeldoRetro avait publié en 2017. Cette fois-ci, Zeldo, principal antagoniste et personnage en image de couverture, compte bien prendre sa revanche et a invité Link à se rendre à la Tour des Souvenirs, où il l'attend de pied ferme.

![Screenshot 1](2019-05-19-zeldo-game-release/screen-2.png)

Vous pouvez télécharger le jeu sur [sa page dans la bibliothèque de quêtes Solarus](/fr/games/defi-de-zeldo-ch-2). Comptez de 4 à 6 heures pour en venir à bout. Amusez-vous bien !

[![Download](2019-05-19-zeldo-game-release/game-download.png)](/fr/games/defi-de-zeldo-ch-2)
