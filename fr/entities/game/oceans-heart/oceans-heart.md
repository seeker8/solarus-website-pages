Lorsque les pirates attaquèrent Calc'île, le père de Tilia décida de les poursuivre afin de sauver les jeunes femmes qu'ils avaient kidnappées. Six mois plus tard, son père n'étant toujours pas revenu, Tilia décida que c'était à elle de le retrouver et de comprendre ce qui s'était passé.

Dirigez Tilia au travers d'un archipel rempli de forteresses pirates, de magie oubliée, de bêtes abominables, et une galerie de personnages essayant tant bien que mal de trouver leur place dans ce monde. À mesure que Tilia remonte la piste de son père, elle se retrouve mêlée à un complot des pirates visant à utiliser une ancienne magie pour contrôler les océans. Et il est possible qu'en chemin elle participe à un tournoi de morpions, qu'elle empêche le vol d'une œuvre d'art, ou qu'elle imite un accordeur de piano. C'est un monde plein de secrets, de dangers et de personnages atypiques que vous aurez à explorer.

### Exploration

Les vagues de la Mer du Nord s'écrasent sur des dizaines d'îles. Pour mener à terme votre quête, vous devrez explorer des forêts touffues, un marais salant, des montagnes escarpées, des cités animées et de nombreux autres lieux. En plus de six grands donjons, les îles sont remplies de mini-donjons et de lieux annexes.

En suivant les chemins non balisés, les tunnels entremêlés, et par delà les routes, vous trouverez des quêtes secrètes, des objets cachés, de nouvelles capacités, et sans doute en apprendrez-vous davantage sur le passé tragique de l'archipel. De vastes zones du monde sont directement libres d'exploration après le début de votre aventure, et au fur et à mesure que Tilia apprendra de nouvelles capacités, le monde s'ouvrira un peu plus.

### Combat

Bien que les îles de la Mer du Nord soient magnifiques, de dangereux monstres se cachent dans leurs recoins les plus sombres. Lorsque vous explorerez des ruines oubliées, des caves sombres ou des collines sauvages, vous tomberez sur des dizaines de monstres qui répandent la corruption. Ils peuvent rapidement vous renvoyer à l'écran de Game-Over, mais de la préparation et de vifs reflexes permettront de garder Tilia en vie et de combattre des pirates sans foi ni loi.

Pendant que vous serez à la recherche du père de Tilia et que vous suivrez d'autres quêtes annexes, vous ne rencontrerez pas moins d'une centaine de types d'ennemis différents à vaincre. En plus de la grande variété de monstres et de pirates peuplant le monde, il y a de nombreuses créatures uniques attendant d'être défiées si vous réussissez à les trouver.

### Développement

Si Tilia veut venir à bout de sa quête, elle devra grandir et devenir une femme forte et ingénieuse. En cherchant parmi des ruines couvertes de mousse et des épaves de navires pirates, vous trouverez des moyens d'accroître la force de Tilia, ainsi que de nombreux objets et pouvoirs magiques qui vous aideront lors des combats et de l'exploration. La plupart des outils que vous obtiendrez auront une utilité à la fois en combat et hors combat. Et tandis que vous pouvez vous attendre à trouver des objets très utiles en explorant les donjons du jeu, c'est bien l'exploration hors des sentiers battus et les quêtes secondaires qui permettront de remplir une grande partie de votre inventaire.

Cependant, Tilia peut seulement utiliser des outils que les autres ont taillé sur mesure pour elle. En combinant des plantes que vous cueillerez et des viscères de monstres récupérées après les combats, Tilia peut préparer des potions qui la rendront plus puissante et plus résistante.

### Intrigues

Les pirates ont formé des alliances et accrû leur pouvoir. Des monstres sanguinaires ont été vus guettant au fond des forêts et des cavernes. Le maire organise une fête pour l'anniversaire de son chien. Il y a autant d'histoires que de personnes vivant sur ces îles. Tilia se retrouvera impliquée dans de nombreuses quêtes, de la plus épique à la plus anecdotique. Les quêtes sont rarement simples, elles peuvent parfois bifurquer ou emprunter une approche non linéaire. C'est à vous de choisir : vous pouvez décider de retrouver le père de Tilia aussi vite que possible, ou bien apprendre les us et coutumes de ce monde, vous impliquer dans les intrigues des personnages et obtenir de l'aide.
