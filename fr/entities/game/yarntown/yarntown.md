### Bienvenue à Yarntown

> *Voici venue la nuit de la chasse. Les rues de cette ville gothique et maudite sont infestées de monstres, et de ce qu'est advenu des hommes qui ont essayé jadis de s'y aventurer. Explorez des chemins tortueux, affrontez de redoutables adversaires, et percez les secrets les plus sombres de Yarntown.*

Yarntown est un **hommage en 2D à Bloodborne**, un jeu vidéo de dark fantasy sorti sur Playstation 4 en 2015. Bloodborne est autant réputé pour ses mécaniques de jeu exigeantes et sans pitié que pour son incroyable atmosphère gothico-victorienne. Cette quête recréé seulement une partie du jeu original, car ce n'est qu'un projet secondaire.

### Contrôles

| Touche   | Action |
|:--------:|--------|
| <kbd>←</kbd> <kbd>↑</kbd> <kbd>→</kbd> <kbd>↓</kbd> | Se déplacer. |
| <kbd>Espace</kbd>                                   | Action / Roulade. |
| <kbd>C</kbd>                                        | Manier votre arme.<br>Maintenez pour charger une attaque. |
| <kbd>X</kbd>                                        | Tirer au pistolet. |
| <kbd>V</kbd>                                        | Utilise une fiole de sang pour se soigner. |
| <kbd>D</kbd>                                        | Mettre le jeu en pause et sauvegarder.<br>La partie reprendra à la dernière lanterne visitée. |

### Mécaniques de jeu

Si vous n'avez pas joué à Bloodborne, voici une brève explication de ses mécaniques de jeu. Préparez-vous à mourir souvent. Vous ne pourrez pas dire qu'on ne vous a pas prévenu(e).

#### Combat

Réaliser des actions consomme votre **barre d'endurance**. Dès lors qu'elle est video, vous ne pourrez plus attaquer ou esquiver. L'attaque chargée consomme davantage d'endurance.

Il est possible d'**étourdir des ennemis**. Si vous attaquez un ennemi dans son dos avec une attaque chargée, cela l'étourdira. Utiliser une attaque normale lorsqu'il est sonné causera une grande quantité de dégâts. On appelle cela une **Attaque viscérale**.

Vous pouvez aussi étourdir un ennemi et ainsi le préparer pour une Attaque viscérale si vous tirez au pistolet juste avant qu'il n'attaque. La fenêtre pour réaliser ceci varie selon l'attaque. Le pistolet n'inflige pas beaucoup de dégâts, donc il est principalement utilisé pour étourdir les ennemis.

Vous pouvez **faire une roulade au travers des attaques ennemies** si vous la déclenchez au bon moment : vous serez invincible quelques secondes, le temps de faire votre roulade.

Les ennemis deviennent très dangereux dès lors qu'ils sont en groupe, puisqu'ils n'attendront pas chacun leur tour pour vous attaquer. **Essayez de préférer les duels en un-contre-un** autant que possible.

#### Santé

Il y a de nombreuses lanternes au travers du niveau de jeu. Vous pouvez choisir de *Communier avec le rêve* au pied de l'une d'entre elles afin de **recharger votre barre de vie**, et déplacer automatiquement des Flacons de Sang et des Cartouches de l'entrepôt vers votre inventaire, jusqu'à 20 chacune. Cependant, reprendre des forces au pied d'une lanterne **ramènera à la vie les ennemis vaincus**.

Vaincre des ennemis vous donnera des Echos de sang. Ils sont utilisés comme monnaie d'échange pour acheter des objets, et aussi pour améliorer les lanternes.

**Quand vous mourrez, la partie reprendra à la dernière lanterne avec laquelle vous avez interagi**. Tous vos Echos de sang resteront là où vous êtes mort. Si vous réussissez à y revenir sans encore mourir, vous pourrez les récupérer. Dans le cas contraire, ils seront perdus pour toujours.

#### Statistiques

Choisissez de *Communier avec le rêve* au pied d'une lanterne pour utiliser vos Echos de sang afin de monter en niveau. Vous pouvez augmenter différentes capacités :

- **Vitalité** augmente votre santé.
- **Endurance** augmente votre endurance.
- **Force** augmente les dégâts que vous infligez et votre défense.
- **Compétence** augmente aussi les dégâts que vous infligez, mais aussi les dégâts de votre pistolet et de vos attaques viscérales.

Vous pouvez trouver des éclats de Pierre de sang, que vous pourrez utiliser sur l'établi non-loin de votre lieu d'arrivée, pour améliorer les dégâts qu'inflige votre arme.

Vous pouvez transporter sur vous jusqu'à 20 Flacons de sang et 20 Balles de mercure à la fois. Si vous en collectez davantage, elles seront automatiquement envoyées à l'entrepôt. Lorsque vous mourez, vos flacons et cartouches seront déplacés de l'entrepôt vers votre inventaire, en respectant la limite de 20.
