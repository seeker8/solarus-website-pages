[container layout="full"]
[game-cover id="{id}"]
[/container]
[highlight]
[space thickness="20"]
[container]

[row]
[column width="1"]
[align type="center"]
[game-icon id="{id}" size="64x64"]
[/align]
[/column]
[column_1 width="8"]
[row_1]

# {title}

[/row_1]
[row_2]

#### {authors}

[/row_2]
[/column_1]

[column_2 width="3"]
[align type="right]
[entity-button-download entity-type="game" id={id} label="Télécharger" label-disabled="En développement" modal-title="Télécharger" modal-button-label="Télécharger"]
[/align]
[/column_2]
[/row]
[/container]
[/highlight]
[container]
[space thickness="0"]

## Captures d'écran

[medias-gallery id="{id}"]

[space]

[row]
[column width="8"]

## Description

{content}

[/column]
[column width="4"]

## Détails

[model id="game-sheet" context-id="{id}"]

[space thickness="30"]

[game-extras id="{id}""]

[/column]
[/row]
[/container]
