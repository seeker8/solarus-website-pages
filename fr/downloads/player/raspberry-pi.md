Pour le moment, la manière la plus simple d'utiliser Solarus sur Raspberry Pi est de passer par RetroPie.

1. Installez (ou mettez à jour) [RetroPie](https://retropie.org.uk/docs/First-Installation/) dans sa dernière version.
2. Dans le menu principal de RetroPie, sélectionnez **Manage Packages** puis **Manage optional packages**.
3. Trouvez le paquet **solarus** et sélectionez l'option **Update from binary**.
4. Téléchargez et [placez les Quêtes](https://retropie.org.uk/docs/Transferring-Roms/) dans le dossier dédié à Solarus (`~/RetroPie/roms/solarus`).
5. Redémarrez votre machine et vos Quêtes seront prêtes à jouer dans EmulationStation.
