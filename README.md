![Solarus Website Pages logo](guidelines/images/logo.png)

# Solarus Website Pages

Solarus Website Pages are the pages that are used for [Solarus official website](https://www.solarus-games.org/). This repository is used conjointly with [Kokori](https://gitlab.com/solarus-games/kokori), our own website engine. The engine parses all these Markdown and JSON files and turn them into HTML pages.

## Guidelines

Remember these guidelines are a work in progress. Ask us on Discord if you need help for anything.

1. [Introduction](guidelines/introduction)
2. [Installation](guidelines/installation)
3. [Contribution](guidelines/contribution)
4. [Files](guidelines/files)
5. [Translation](guidelines/translation)
6. [Adding a game to the library](guidelines/game)

## Scripts

Creating files is very tedious. Some [scripts](scripts/Scripts) are here to update JSON and MD files.
