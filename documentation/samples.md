[container]
##Titles
#Title level 1
##Title level 2
###Title level 3
####Title level 4
[space thickness="50"][/space]
## Paragraphs
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[space thickness="50"][/space]
##Alerts
[alert type="primary" dismiss="true" title="Primary"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="secondary" dismiss="true" title="Secondary"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="info" dismiss="true" title="Info"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="success" dismiss="true" title="Success"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="warning" dismiss="true" title="Warning"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="danger" dismiss="true" title="Danger"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="light" dismiss="true" title="Light"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="dark" dismiss="true" title="Dark"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="primary" dismiss="true" icon-category="fab" icon="windows"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="primary" dismiss="true" title="With title"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="secondary" dismiss="true" title="With icon and title" icon-category="fab" icon="windows"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="primary" title="Without dismiss action"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[space thickness="50"][/space]
##Badges
[badge type="primary" label="Primary"][/badge]
[badge type="secondary" label="Primary"][/badge]
[badge type="info" label="Info"][/badge]
[badge type="success" label="Success"][/badge]
[badge type="warning" label="Warning"][/badge]
[badge type="danger" label="Danger"][/badge]
[badge type="light" label="Light"][/badge]
[badge type="dark" label="Dark"][/badge]
[badge type="primary" icon-category="fab" icon="windows" label="With icon"][/badge]
[space thickness="50"][/space]
##Buttons
[button type="primary" label="Primary"][/button]
[button type="secondary" label="Secondary"][/button]
[button type="info" label="Info"][/button]
[button type="success" label="Success"][/button]
[button type="warning" label="Warning"][/button]
[button type="danger" label="Danger"][/button]
[button type="light" label="Light"][/button]
[button type="dark" label="Dark"][/button]
[space][/space]
[button type="primary" outline="true" label="Primary outline"][/button]
[button type="secondary" outline="true" label="Secondary outline"][/button]
[button type="info" outline="true" label="Info outline"][/button]
[button type="success" outline="true" label="Success outline"][/button]
[space][/space]
[button type="warning" outline="true" label="Warning outline"][/button]
[button type="danger" outline="true" label="Danger outline"][/button]
[button type="light" outline="true" label="Light outline"][/button]
[button type="dark" outline="true" label="Dark outline"][/button]
[space][/space]
[button type="primary" size="lg" label="Large"][/button]
[button type="primary" size="md" label="Medium"][/button]
[button type="primary" size="sm" label="Small"][/button]
[space][/space]
[button type="primary" icon-category="fab" icon="linux" label="With icon"][/button]
[button type="secondary" icon-category="fab" icon="apple" outline="true" label="With icon outline"][/button]
[space thickness="50"][/space]
##Icons
[icon icon="apple" type="primary" category="fab"][/icon]
[icon icon="apple" type="secondary" category="fab"][/icon]
[icon icon="apple" type="info" category="fab"][/icon]
[icon icon="apple" type="success" category="fab"][/icon]
[icon icon="apple" type="warning" category="fab"][/icon]
[icon icon="apple" type="danger" category="fab"][/icon]
[icon icon="apple" type="light" category="fab"][/icon]
[icon icon="apple" type="dark" category="fab"][/icon]
[space][/space]
[icon icon="apple" size="2x" category="fab"][/icon]
[icon icon="apple" size="3x" category="fab"][/icon]
[icon icon="apple" size="4x" category="fab"][/icon]
[icon icon="apple" size="5x" category="fab"][/icon]
[icon icon="apple" size="6x" category="fab"][/icon]
[icon icon="apple" size="7x" category="fab"][/icon]
[icon icon="apple" size="8x" category="fab"][/icon]
[icon icon="apple" size="9x" category="fab"][/icon]
[icon icon="apple" size="10x" category="fab"][/icon]
[icon icon="apple" bordered="true" category="fab"][/icon]
[icon icon="apple" mask="circle" category="fab"][/icon]
[space thickness="50"][/space]
##Progressbars
[progressbar type="primary" width="100"]
Primary
[/progressbar]
[space][/space]
[progressbar type="secondary" width="90"]
Secondary
[/progressbar]
[space][/space]
[progressbar type="info" width="80"]
Info
[/progressbar]
[space][/space]
[progressbar type="success" width="70"]
Success
[/progressbar]
[space][/space]
[progressbar type="warning" width="60"]
Warning
[/progressbar]
[space][/space]
[progressbar type="danger" width="50"]
Danger
[/progressbar]
[space][/space]
[progressbar type="light" width="50"]
Light
[/progressbar]
[space][/space]
[progressbar type="dark" width="50"]
Dark
[/progressbar]
[space][/space]
[progressbar type="primary" striped="true" width="40"]
Striped
[/progressbar]
[space][/space]
[progressbar type="primary" striped="true" animated="true" width="30"]
Striped and animated
[/progressbar]
[/container]