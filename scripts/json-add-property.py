# Simple Python script to walk JSON files recursively and add a property to
# every one, with a default value. Handles relative file values.
# Example:
# python scripts/json-add-property.py -root "fr/entities/article/old/2017" -name "meta_image" -value "assets/images/meta_image_fallback.jpg" -f -v -r

import glob
import json
import os
import argparse

import jsonutils

if __name__ == '__main__':
  # Create parameter parser
  parser = argparse.ArgumentParser(description="Simple script to add a property and a default value to JSON files.")
  parser.add_argument("-root", type=str, help="Folder to process")
  parser.add_argument("-name", type=str, help="Property name to add")
  parser.add_argument("-value", help="Property value to add")
  parser.add_argument("-f", "--file_value", help="If the value is a file path", action="store_true")
  parser.add_argument("-v", "--verbose", help="Increase output verbosity", action="store_true")
  parser.add_argument("-o", "--override", help="If the property is already present, overrides its value", action="store_true")
  parser.add_argument("-r", "--recursive", help="If the subfolders must also be processed", action="store_true")

  # Parse parameters
  args = parser.parse_args()

  # Print parameters if verbose
  if args.verbose:
    print("Verbose mode enabled.\n")
    print("Root:                              " + str(args.root))
    print("Recursive:                         " + str(args.recursive))
    print("Property to add:                   " + str(args.name))
    print("Property value to add:             " + str(args.value))
    print("Property value is a relative path: " + str(args.file_value))
    print("Property value will be overriden:  " + str(args.override))

  # Run
  def process_callback(file_path, property_name):
    return jsonutils.add_property_to_file(file_path, property_name, args.value, property_is_file=args.file_value)
  
  jsonutils.process_all_files(args.root, args.name, process_callback, recursive=args.recursive, verbosity=args.verbose)
