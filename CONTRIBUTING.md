# How to contribute

Thank you for reading this, because we need volunteers to add content to this website.

## Resources

Here are some important resources:

  * [Kokori](https://gitlab.com/solarus-games/kokori), the engine that powers Solarus' website.
  * [Bugtracker](https://gitlab.com/solarus-games/solarus-website-pages/issues), to report bugs or errors.
  * [Discord channel](https://discord.gg/qR7gPBV), to talk about modifications.

## Tutorial

1. [Introduction](guidelines/introduction.md)
2. [Installation](guidelines/installation.md)
3. [Contribution](guidelines/contribution.md)
4. [Files](guidelines/files.md)
5. [Translation](guidelines/translation.md)
6. [Adding a game to the library](guidelines/game.md)
